package br.edu.unisep.orderservise.domain.usecase.funcionario;

import br.edu.unisep.orderservise.data.repository.FuncionarioRepository;
import br.edu.unisep.orderservise.domain.builder.funcionario.FuncionarioBuilder;
import br.edu.unisep.orderservise.domain.dto.funcionario.RegisterFuncionarioDto;
import br.edu.unisep.orderservise.domain.validator.funcionario.RegisterFuncionarioValidator;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class RegisterFuncionarioUseCase {

    private final RegisterFuncionarioValidator validator;
    private final FuncionarioBuilder builder;
    private final FuncionarioRepository funcionarioRepository;

    public void execute(RegisterFuncionarioDto registerFuncionario) {
        validator.validate(registerFuncionario);

        var funcionario = builder.from(registerFuncionario);
        funcionarioRepository.save(funcionario);
    }
}
