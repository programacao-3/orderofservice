package br.edu.unisep.orderservise.domain.usecase.funcionario;

import br.edu.unisep.orderservise.data.repository.FuncionarioRepository;
import br.edu.unisep.orderservise.domain.builder.funcionario.FuncionarioBuilder;
import br.edu.unisep.orderservise.domain.dto.funcionario.FuncionarioDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class FindAllFuncionariosUseCase {

    private final FuncionarioRepository funcionarioRepository;
    private final FuncionarioBuilder funcionarioBuilder;

    public List<FuncionarioDto> execute() {
        var funcionario = funcionarioRepository.findAll();
        return funcionarioBuilder.from(funcionario);

    }
}
