package br.edu.unisep.orderservise.domain.usecase.carro;

import br.edu.unisep.orderservise.data.repository.CarroRepository;
import br.edu.unisep.orderservise.domain.builder.carro.CarroBuilder;
import br.edu.unisep.orderservise.domain.dto.carro.CarroDto;
import br.edu.unisep.orderservise.domain.validator.carro.FindCarroByIdValidator;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class FindCarroByIdUseCase {

    private final FindCarroByIdValidator validator;
    private final CarroRepository carroRepository;
    private final CarroBuilder builder;

    public CarroDto execute(Integer caroId) {
        validator.validate(caroId);

        var carro = carroRepository.findById(caroId);
        return carro.map(builder::from).orElse(null);
    }
}
