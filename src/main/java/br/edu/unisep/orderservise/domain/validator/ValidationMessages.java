package br.edu.unisep.orderservise.domain.validator;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ValidationMessages {

    //FUNCIONARIO
    public static final String MESSAGE_REQUIRED_FUNCIONARIO_ID = "Informe o id do funcionário!";
    public static final String MESSAGE_REQUIRED_FUNCINARIO_NAME = "Informe o nome do funcionário!";

    //CLIENTE
    public static final String MESSAGE_REQUIRED_CLIENTE_ID = "Informe o Id do cliente!";
    public static final String MESSAGE_REQUIRED_CLIENTE_NOME = "Informe o nome do cliente!";
    public static final String MESSAGE_REQUIRED_CLIENTE_FONE = "Informe o telefone do cliente!";
    public static final String MESSAGE_REQUIRED_CLIENTE_CPF = "Informe o CPF do cliente!";
    public static final String MESSAGE_REQUIRED_CLIENTE_ENDERECO = "Informe o endereço do cliente!";

    //CARRO
    public static final String MESSAGE_REQUIRED_CARRO_ID = "Informe o Id do carro!";
    public static final String MESSAGE_REQUIRED_CARRO_CLIENTE = "Informe o Proprietario do carro!";
    public static final String MESSAGE_REQUIRED_CARRO_MODELO = "Informe o modelo do carro!";
    public static final String MESSAGE_REQUIRED_CARRO_PLACA = "Informe a placa do carro!";

    //ORDEM DE SERVIÇO
    public static final String MESSAGE_REQUIRED_ORDEMDESERVICO_ID = "Informe o Id da Ordem de serviço";
    public static final String MESSAGE_REQUIRED_ORDEMDESERVICO_DESCRICAO = "'Informe a descrição do serviço";
    public static final String MESSAGE_REQUIRED_ORDEMDESERVICO_MAODEOBRA = "'Informe a mão de obra do serviço";
    public static final String MESSAGE_REQUIRED_ORDEMDESERVICO_VALOR = "'Informe o valor do serviço";
}
