package br.edu.unisep.orderservise.domain.dto.carro;

import br.edu.unisep.orderservise.data.entity.cliente.Cliente;
import lombok.AllArgsConstructor;
import lombok.Getter;


@Getter
@AllArgsConstructor
public class CarroDto {

    private final Integer id;

    private final Cliente cliente;

    private final String modelo;

    private final String placa;
}
