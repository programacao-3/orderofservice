package br.edu.unisep.orderservise.domain.usecase.carro;

import br.edu.unisep.orderservise.data.repository.CarroRepository;
import br.edu.unisep.orderservise.domain.builder.carro.CarroBuilder;
import br.edu.unisep.orderservise.domain.dto.carro.CarroDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class FindAllCarroUseCase {

    private final CarroRepository carroRepository;
    private final CarroBuilder carroBuilder;

    public List<CarroDto> execute() {
        var carro = carroRepository.findAll();
        return carroBuilder.from(carro);

    }
}
