package br.edu.unisep.orderservise.domain.usecase.carro;

import br.edu.unisep.orderservise.data.repository.CarroRepository;;
import br.edu.unisep.orderservise.domain.builder.carro.CarroBuilder;
import br.edu.unisep.orderservise.domain.dto.carro.RegisterCarroDto;
import br.edu.unisep.orderservise.domain.validator.carro.RegisterCarroValidator;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class RegisterCarroUseCase {

    private final RegisterCarroValidator validator;
    private final CarroBuilder builder;
    private final  CarroRepository carroRepository;

    public void execute(RegisterCarroDto registerCarro) {
        validator.validate(registerCarro);

        var carro = builder.from(registerCarro);
        carroRepository.save(carro);
    }
}



