package br.edu.unisep.orderservise.domain.builder.cliente;

import br.edu.unisep.orderservise.data.entity.cliente.Cliente;
import br.edu.unisep.orderservise.domain.dto.cliente.ClienteDto;
import br.edu.unisep.orderservise.domain.dto.cliente.RegisterClienteDto;
import br.edu.unisep.orderservise.domain.dto.cliente.UpdateClienteDto;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ClienteBuilder {

    public List<ClienteDto> from(List<Cliente> clientes) {
        return clientes.stream().map(this::from).collect(Collectors.toList());
    }

    public ClienteDto from(Cliente cliente) {
        return new ClienteDto(
                cliente.getId(),
                cliente.getNome(),
                cliente.getFone(),
                cliente.getCpf(),
                cliente.getEndereco()
        );
    }

    public Cliente from(UpdateClienteDto updateCliente) {
        Cliente cliente = from((RegisterClienteDto) updateCliente);
        cliente.setId(updateCliente.getId());

        return cliente;
    }

    public Cliente from(RegisterClienteDto registerClienteDto) {
        Cliente cliente = new Cliente();
        cliente.setNome(registerClienteDto.getNome());
        cliente.setFone(registerClienteDto.getFone());
        cliente.setCpf(registerClienteDto.getCpf());
        cliente.setEndereco(registerClienteDto.getEndereco());

        return cliente;
    }

}
