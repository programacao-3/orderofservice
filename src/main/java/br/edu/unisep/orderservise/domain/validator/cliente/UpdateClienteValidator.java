package br.edu.unisep.orderservise.domain.validator.cliente;

import br.edu.unisep.orderservise.domain.dto.cliente.UpdateClienteDto;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.Validate;
import org.springframework.stereotype.Component;

import static br.edu.unisep.orderservise.domain.validator.ValidationMessages.MESSAGE_REQUIRED_CLIENTE_ID;

@Component
@AllArgsConstructor
public class UpdateClienteValidator {

    private final RegisterClienteValidator registerValidator;

    public void validate(UpdateClienteDto cliente) {
        Validate.notNull(cliente.getId(), MESSAGE_REQUIRED_CLIENTE_ID);
        Validate.isTrue(cliente.getId() > 0, MESSAGE_REQUIRED_CLIENTE_ID);

        registerValidator.validate(cliente);
    }

}
