package br.edu.unisep.orderservise.domain.validator.funcionario;

import br.edu.unisep.orderservise.domain.dto.funcionario.RegisterFuncionarioDto;
import static br.edu.unisep.orderservise.domain.validator.ValidationMessages.*;
import org.apache.commons.lang3.Validate;
import org.springframework.stereotype.Component;

@Component
public class RegisterFuncionarioValidator {
    public void validate(RegisterFuncionarioDto registerFuncionario) {

        Validate.notBlank(registerFuncionario.getNome(), MESSAGE_REQUIRED_FUNCINARIO_NAME);

    }
}
