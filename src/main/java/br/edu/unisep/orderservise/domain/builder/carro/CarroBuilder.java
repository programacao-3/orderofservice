package br.edu.unisep.orderservise.domain.builder.carro;

import br.edu.unisep.orderservise.data.entity.carro.Carro;
import br.edu.unisep.orderservise.domain.dto.carro.CarroDto;
import br.edu.unisep.orderservise.domain.dto.carro.RegisterCarroDto;
import br.edu.unisep.orderservise.domain.dto.carro.UpdateCarroDto;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;
@Component
public class CarroBuilder {

    public List<CarroDto> from(List<Carro> carros) {
        return carros.stream().map(this::from).collect(Collectors.toList());}

    public CarroDto from(Carro carros){
        return new CarroDto(
                carros.getId(),
                carros.getCliente(),
                carros.getModelo(),
                carros.getPlaca()
        );
    }

    public Carro from(RegisterCarroDto registerCarroDto){
        Carro carro = new Carro();
        carro.setCliente(registerCarroDto.getCliente());
        carro.setModelo(registerCarroDto.getModelo());
        carro.setPlaca(registerCarroDto.getPlaca());

        return carro;
    }

    public Carro from(UpdateCarroDto updateCarro){
        Carro carro = from((RegisterCarroDto) updateCarro);
        carro.setId(updateCarro.getId());

        return carro;
    }

}
