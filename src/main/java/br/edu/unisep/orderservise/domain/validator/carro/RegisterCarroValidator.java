package br.edu.unisep.orderservise.domain.validator.carro;

import br.edu.unisep.orderservise.domain.dto.carro.RegisterCarroDto;
import org.apache.commons.lang3.Validate;
import org.springframework.stereotype.Component;

import static br.edu.unisep.orderservise.domain.validator.ValidationMessages.MESSAGE_REQUIRED_CARRO_MODELO;

@Component
public class RegisterCarroValidator {
    public void validate(RegisterCarroDto registerCarro) {

        Validate.notBlank(registerCarro.getModelo(), MESSAGE_REQUIRED_CARRO_MODELO);

    }

}
