package br.edu.unisep.orderservise.domain.validator.funcionario;

import br.edu.unisep.orderservise.domain.dto.funcionario.UpdateFuncionarioDto;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.Validate;
import org.springframework.stereotype.Component;

import static br.edu.unisep.orderservise.domain.validator.ValidationMessages.*;

@Component
@AllArgsConstructor
public class UpdateFuncionarioValidator {

    private final RegisterFuncionarioValidator registerValidator;

    public void validate(UpdateFuncionarioDto funcionario){
        Validate.notNull(funcionario.getId(), MESSAGE_REQUIRED_FUNCIONARIO_ID);
        Validate.isTrue(funcionario.getId() > 0, MESSAGE_REQUIRED_FUNCIONARIO_ID);

        registerValidator.validate(funcionario);
    }
}
