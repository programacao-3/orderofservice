package br.edu.unisep.orderservise.domain.usecase.cliente;

import br.edu.unisep.orderservise.data.repository.ClienteRepository;
import br.edu.unisep.orderservise.domain.builder.cliente.ClienteBuilder;
import br.edu.unisep.orderservise.domain.dto.cliente.ClienteDto;
import br.edu.unisep.orderservise.domain.validator.cliente.FindClienteByIdValidator;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class FindClienteByIdUseCase {

    private final FindClienteByIdValidator validator;
    private final ClienteRepository clienteRepository;
    private final ClienteBuilder builder;

    public ClienteDto execute(Integer clienteId) {
        validator.validate(clienteId);

        var cliente = clienteRepository.findById(clienteId);
        return cliente.map(builder::from).orElse(null);
    }

}
