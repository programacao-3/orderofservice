package br.edu.unisep.orderservise.domain.dto.funcionario;

import lombok.Data;

@Data
public class UpdateFuncionarioDto extends RegisterFuncionarioDto {

    private Integer id;
}
