package br.edu.unisep.orderservise.domain.dto.funcionario;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegisterFuncionarioDto {

    private String nome;
}
