package br.edu.unisep.orderservise.domain.usecase.funcionario;

import br.edu.unisep.orderservise.data.repository.FuncionarioRepository;
import br.edu.unisep.orderservise.domain.builder.funcionario.FuncionarioBuilder;
import br.edu.unisep.orderservise.domain.dto.funcionario.UpdateFuncionarioDto;
import br.edu.unisep.orderservise.domain.validator.funcionario.UpdateFuncionarioValidator;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class UpdateFuncionarioUseCase {


    private final UpdateFuncionarioValidator validator;
    private final FuncionarioBuilder builder;
    private final FuncionarioRepository funcionarioRepository;

    public void execute(UpdateFuncionarioDto updateFuncionario) {
        validator.validate(updateFuncionario);

        var funcionario = builder.from(updateFuncionario);
        funcionarioRepository.save(funcionario);
    }

}
