package br.edu.unisep.orderservise.domain.dto.cliente;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegisterClienteDto {

    private String nome;

    private String fone;

    private String cpf;

    private String endereco;

}
