package br.edu.unisep.orderservise.domain.validator.cliente;

import org.apache.commons.lang3.Validate;
import org.springframework.stereotype.Component;

import static br.edu.unisep.orderservise.domain.validator.ValidationMessages.MESSAGE_REQUIRED_CLIENTE_ID;

@Component
public class FindClienteByIdValidator {

    public void validate(Integer id) {
        Validate.isTrue(id > 0, MESSAGE_REQUIRED_CLIENTE_ID);
    }

}
