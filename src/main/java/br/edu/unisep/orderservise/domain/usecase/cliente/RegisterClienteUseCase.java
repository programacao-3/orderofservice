package br.edu.unisep.orderservise.domain.usecase.cliente;

import br.edu.unisep.orderservise.data.repository.ClienteRepository;
import br.edu.unisep.orderservise.domain.builder.cliente.ClienteBuilder;
import br.edu.unisep.orderservise.domain.dto.cliente.RegisterClienteDto;
import br.edu.unisep.orderservise.domain.validator.cliente.RegisterClienteValidator;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class RegisterClienteUseCase {

    private final RegisterClienteValidator validator;
    private final ClienteBuilder builder;
    private final ClienteRepository clienteRepository;

    public void execute(RegisterClienteDto registerClienteDto) {
        validator.validate(registerClienteDto);

        var cliente = builder.from(registerClienteDto);
        clienteRepository.save(cliente);
    }



}
