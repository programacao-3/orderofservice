package br.edu.unisep.orderservise.domain.builder.funcionario;

import br.edu.unisep.orderservise.data.entity.funcionario.Funcionario;
import br.edu.unisep.orderservise.domain.dto.funcionario.FuncionarioDto;
import br.edu.unisep.orderservise.domain.dto.funcionario.RegisterFuncionarioDto;
import br.edu.unisep.orderservise.domain.dto.funcionario.UpdateFuncionarioDto;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class FuncionarioBuilder {

    public List<FuncionarioDto> from(List<Funcionario> funcionarios) {
        return funcionarios.stream().map(this::from).collect(Collectors.toList());}

    public FuncionarioDto from(Funcionario funcionario){
        return new FuncionarioDto(
            funcionario.getId(),
            funcionario.getNome()
        );
    }

    public Funcionario from(RegisterFuncionarioDto registerFuncionarioDto){
        Funcionario funcionario = new Funcionario();
        funcionario.setNome(registerFuncionarioDto.getNome());

        return funcionario;
    }

    public Funcionario from(UpdateFuncionarioDto updateFuncionarioDto){
        Funcionario funcionario = from((RegisterFuncionarioDto) updateFuncionarioDto);
        funcionario.setId(updateFuncionarioDto.getId());

        return funcionario;
    }
}
