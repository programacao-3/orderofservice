package br.edu.unisep.orderservise.domain.dto.carro;

import br.edu.unisep.orderservise.data.entity.cliente.Cliente;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegisterCarroDto {

    private Cliente cliente;
    private String modelo;
    private String placa;
}
