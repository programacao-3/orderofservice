package br.edu.unisep.orderservise.domain.validator.carro;

import br.edu.unisep.orderservise.domain.dto.carro.UpdateCarroDto;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.Validate;
import org.springframework.stereotype.Component;
import static br.edu.unisep.orderservise.domain.validator.ValidationMessages.MESSAGE_REQUIRED_CARRO_ID;

@Component
@AllArgsConstructor
public class UpdateCarroValidator {
    private final RegisterCarroValidator registerValidator;

    public void validate(UpdateCarroDto carro){
        Validate.notNull(carro.getId(), MESSAGE_REQUIRED_CARRO_ID);
        Validate.isTrue(carro.getId() > 0, MESSAGE_REQUIRED_CARRO_ID);

        registerValidator.validate(carro);
    }
}
