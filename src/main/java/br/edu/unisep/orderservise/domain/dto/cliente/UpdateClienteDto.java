package br.edu.unisep.orderservise.domain.dto.cliente;

import lombok.Data;

@Data
public class UpdateClienteDto extends RegisterClienteDto{

    private Integer id;

}
