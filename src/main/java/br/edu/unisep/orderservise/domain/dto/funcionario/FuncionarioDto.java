package br.edu.unisep.orderservise.domain.dto.funcionario;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class FuncionarioDto {

    private final Integer id;

    private final String nome;
}
