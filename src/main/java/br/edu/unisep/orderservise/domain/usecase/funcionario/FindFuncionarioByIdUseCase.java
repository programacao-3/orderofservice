package br.edu.unisep.orderservise.domain.usecase.funcionario;

import br.edu.unisep.orderservise.data.repository.FuncionarioRepository;
import br.edu.unisep.orderservise.domain.builder.funcionario.FuncionarioBuilder;
import br.edu.unisep.orderservise.domain.dto.funcionario.FuncionarioDto;
import br.edu.unisep.orderservise.domain.validator.funcionario.FindFuncionarioByIdValidator;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class FindFuncionarioByIdUseCase {

    private final FindFuncionarioByIdValidator validator;
    private final FuncionarioRepository funcionarioRepository;
    private final FuncionarioBuilder builder;

    public FuncionarioDto execute(Integer funcionarioId) {
        validator.validate(funcionarioId);

        var funcionario = funcionarioRepository.findById(funcionarioId);
        return funcionario.map(builder::from).orElse(null);
    }
}
