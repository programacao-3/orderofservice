package br.edu.unisep.orderservise.domain.usecase.cliente;

import br.edu.unisep.orderservise.data.repository.ClienteRepository;
import br.edu.unisep.orderservise.domain.builder.cliente.ClienteBuilder;
import br.edu.unisep.orderservise.domain.dto.cliente.UpdateClienteDto;
import br.edu.unisep.orderservise.domain.validator.cliente.UpdateClienteValidator;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class UpdateClienteUseCase {

    private final UpdateClienteValidator validator;
    private final ClienteBuilder builder;
    private final ClienteRepository clienteRepository;

    public void execute(UpdateClienteDto updateClienteDto) {
        validator.validate(updateClienteDto);

        var cliente = builder.from(updateClienteDto);
        clienteRepository.save(cliente);
    }



}
