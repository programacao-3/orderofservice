package br.edu.unisep.orderservise.domain.validator.carro;

import org.apache.commons.lang3.Validate;
import org.springframework.stereotype.Component;

import static br.edu.unisep.orderservise.domain.validator.ValidationMessages.MESSAGE_REQUIRED_CARRO_ID;

@Component
public class FindCarroByIdValidator {

    public void validate(Integer id){
        Validate.isTrue(id > 0, MESSAGE_REQUIRED_CARRO_ID);
    }
}
