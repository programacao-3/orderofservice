package br.edu.unisep.orderservise.domain.dto.carro;

import lombok.Data;

@Data
public class UpdateCarroDto extends RegisterCarroDto {

    private Integer id;
}
