package br.edu.unisep.orderservise.domain.dto.cliente;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ClienteDto {

    private Integer id;

    private String nome;

    private String fone;

    private String cpf;

    private String endereco;

}
