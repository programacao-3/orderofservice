package br.edu.unisep.orderservise.domain.usecase.carro;

import br.edu.unisep.orderservise.data.repository.CarroRepository;
import br.edu.unisep.orderservise.domain.builder.carro.CarroBuilder;
import br.edu.unisep.orderservise.domain.dto.carro.UpdateCarroDto;
import br.edu.unisep.orderservise.domain.validator.carro.UpdateCarroValidator;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class UpdateCarroUseCase {


    private final UpdateCarroValidator validator;
    private final CarroBuilder builder;
    private final CarroRepository carroRepository;

    public void execute(UpdateCarroDto updateCarro) {
        validator.validate(updateCarro);

        var carro = builder.from(updateCarro);
        carroRepository.save(carro);
    }
}