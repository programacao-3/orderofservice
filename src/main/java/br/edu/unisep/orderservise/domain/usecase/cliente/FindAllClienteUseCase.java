package br.edu.unisep.orderservise.domain.usecase.cliente;


import br.edu.unisep.orderservise.data.repository.ClienteRepository;
import br.edu.unisep.orderservise.domain.builder.cliente.ClienteBuilder;
import br.edu.unisep.orderservise.domain.dto.cliente.ClienteDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class FindAllClienteUseCase {

    private final ClienteRepository clienteRepository;
    private final ClienteBuilder clienteBuilder;

    public List<ClienteDto> execute() {
        var clientes = clienteRepository.findAll();
        return clienteBuilder.from(clientes);
    }

}
