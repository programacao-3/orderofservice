package br.edu.unisep.orderservise.domain.validator.funcionario;

import org.apache.commons.lang3.Validate;
import org.springframework.stereotype.Component;
import static br.edu.unisep.orderservise.domain.validator.ValidationMessages.MESSAGE_REQUIRED_FUNCIONARIO_ID;

@Component
public class FindFuncionarioByIdValidator {

    public void validate(Integer id){
        Validate.isTrue(id > 0, MESSAGE_REQUIRED_FUNCIONARIO_ID);
    }
}
