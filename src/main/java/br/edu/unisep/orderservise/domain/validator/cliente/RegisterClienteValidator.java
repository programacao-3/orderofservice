package br.edu.unisep.orderservise.domain.validator.cliente;

import br.edu.unisep.orderservise.domain.dto.cliente.RegisterClienteDto;
import org.apache.commons.lang3.Validate;
import org.springframework.stereotype.Component;

import static br.edu.unisep.orderservise.domain.validator.ValidationMessages.*;

@Component
public class RegisterClienteValidator {

    public void validate(RegisterClienteDto registerClienteDto) {
        Validate.notNull(registerClienteDto.getNome(), MESSAGE_REQUIRED_CLIENTE_NOME);
        Validate.notNull(registerClienteDto.getFone(), MESSAGE_REQUIRED_CLIENTE_FONE);
        Validate.notNull(registerClienteDto.getCpf(), MESSAGE_REQUIRED_CLIENTE_CPF);
        Validate.notNull(registerClienteDto.getEndereco(),MESSAGE_REQUIRED_CLIENTE_ENDERECO);
    }

}
