package br.edu.unisep.orderservise.controller.cliente;

import br.edu.unisep.orderservise.domain.dto.cliente.ClienteDto;
import br.edu.unisep.orderservise.domain.dto.cliente.RegisterClienteDto;
import br.edu.unisep.orderservise.domain.dto.cliente.UpdateClienteDto;
import br.edu.unisep.orderservise.domain.usecase.cliente.FindAllClienteUseCase;
import br.edu.unisep.orderservise.domain.usecase.cliente.FindClienteByIdUseCase;
import br.edu.unisep.orderservise.domain.usecase.cliente.RegisterClienteUseCase;
import br.edu.unisep.orderservise.domain.usecase.cliente.UpdateClienteUseCase;
import br.edu.unisep.orderservise.response.DefaultResponse;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/cliente")
public class ClienteController {

    private final FindAllClienteUseCase findAllCliente;
    private final FindClienteByIdUseCase findById;
    private final RegisterClienteUseCase registerCliente;
    private final UpdateClienteUseCase updateCliente;

    @GetMapping
    public ResponseEntity<DefaultResponse<List<ClienteDto>> > findAll() {
        var clientes = findAllCliente.execute();

        return clientes.isEmpty() ?
                ResponseEntity.noContent().build() :
                ResponseEntity.ok(DefaultResponse.of(clientes));
    }

    @GetMapping("/{id}")
    public ResponseEntity<DefaultResponse<ClienteDto>> findById(@PathVariable("id") Integer id) {
        var cliente = findById.execute(id);

        return cliente == null ?
                ResponseEntity.notFound().build() :
                ResponseEntity.ok(DefaultResponse.of(cliente));
    }

    @PostMapping
    public ResponseEntity<DefaultResponse<Boolean>> save(@RequestBody RegisterClienteDto  cliente) {
        registerCliente.execute(cliente);
        return ResponseEntity.ok(DefaultResponse.of(true));
    }

    @PutMapping
    public ResponseEntity<DefaultResponse<Boolean>> update(@RequestBody UpdateClienteDto cliente) {
        updateCliente.execute(cliente);
        return ResponseEntity.ok(DefaultResponse.of(true));
    }

    
}
