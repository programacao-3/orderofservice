package br.edu.unisep.orderservise.controller.funcionario;

import br.edu.unisep.orderservise.domain.dto.carro.CarroDto;
import br.edu.unisep.orderservise.domain.dto.carro.RegisterCarroDto;
import br.edu.unisep.orderservise.domain.dto.carro.UpdateCarroDto;
import br.edu.unisep.orderservise.domain.usecase.carro.FindAllCarroUseCase;
import br.edu.unisep.orderservise.domain.usecase.carro.FindCarroByIdUseCase;

import br.edu.unisep.orderservise.domain.usecase.carro.RegisterCarroUseCase;
import br.edu.unisep.orderservise.domain.usecase.carro.UpdateCarroUseCase;
import br.edu.unisep.orderservise.response.DefaultResponse;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/carro")
public class CarroController {

    private final FindAllCarroUseCase findAllCarroValidator;
    private final FindCarroByIdUseCase findById;
    private final RegisterCarroUseCase registerCarro;
    private final UpdateCarroUseCase updateCarro;

    @GetMapping
    public ResponseEntity<DefaultResponse<List<CarroDto>>> findAll() {
        var carro = findAllCarroValidator.execute();

        return carro.isEmpty() ?
                ResponseEntity.noContent().build() :
                ResponseEntity.ok(DefaultResponse.of(carro));
    }

    @GetMapping("/{id}")
    public ResponseEntity<DefaultResponse<CarroDto>> findById(@PathVariable("id") Integer id) {
        var carro = findById.execute(id);

        return carro == null ?
                ResponseEntity.notFound().build() :
                ResponseEntity.ok(DefaultResponse.of(carro));
    }

    @PostMapping
    public ResponseEntity<DefaultResponse<Boolean>> save(@RequestBody RegisterCarroDto carro) {
        registerCarro.execute(carro);
        return ResponseEntity.ok(DefaultResponse.of(true));
    }

    @PutMapping
    public ResponseEntity<DefaultResponse<Boolean>> update(@RequestBody UpdateCarroDto carro) {
        updateCarro.execute(carro);
        return ResponseEntity.ok(DefaultResponse.of(true));
    }
}
