package br.edu.unisep.orderservise.controller.funcionario;

import br.edu.unisep.orderservise.domain.dto.funcionario.FuncionarioDto;
import br.edu.unisep.orderservise.domain.dto.funcionario.RegisterFuncionarioDto;
import br.edu.unisep.orderservise.domain.dto.funcionario.UpdateFuncionarioDto;
import br.edu.unisep.orderservise.domain.usecase.funcionario.FindAllFuncionariosUseCase;
import br.edu.unisep.orderservise.domain.usecase.funcionario.FindFuncionarioByIdUseCase;
import br.edu.unisep.orderservise.domain.usecase.funcionario.RegisterFuncionarioUseCase;
import br.edu.unisep.orderservise.domain.usecase.funcionario.UpdateFuncionarioUseCase;
import br.edu.unisep.orderservise.response.DefaultResponse;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/funcionario")
public class FuncionarioController {

    private final FindAllFuncionariosUseCase findAllFuncionarios;
    private final FindFuncionarioByIdUseCase findById;
    private final RegisterFuncionarioUseCase registerFuncionario;
    private final UpdateFuncionarioUseCase updateFuncionario;


    @GetMapping
    public ResponseEntity<DefaultResponse<List<FuncionarioDto>>> findAll() {
        var funcionarios = findAllFuncionarios.execute();

        return funcionarios.isEmpty() ?
                ResponseEntity.noContent().build() :
                ResponseEntity.ok(DefaultResponse.of(funcionarios));
    }

    @GetMapping("/{id}")
    public ResponseEntity<DefaultResponse<FuncionarioDto>> findById(@PathVariable("id") Integer id) {
        var funcionario = findById.execute(id);

        return funcionario == null ?
                ResponseEntity.notFound().build() :
                ResponseEntity.ok(DefaultResponse.of(funcionario));
    }

    @PostMapping
    public ResponseEntity<DefaultResponse<Boolean>> save(@RequestBody RegisterFuncionarioDto funcionario) {
        registerFuncionario.execute(funcionario);
        return ResponseEntity.ok(DefaultResponse.of(true));
    }

    @PutMapping
    public ResponseEntity<DefaultResponse<Boolean>> update(@RequestBody UpdateFuncionarioDto funcionario) {
        updateFuncionario.execute(funcionario);
        return ResponseEntity.ok(DefaultResponse.of(true));
    }
}
