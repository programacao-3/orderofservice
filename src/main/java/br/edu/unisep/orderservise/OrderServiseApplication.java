package br.edu.unisep.orderservise;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OrderServiseApplication {

	public static void main(String[] args) {
		SpringApplication.run(OrderServiseApplication.class, args);
	}

}
