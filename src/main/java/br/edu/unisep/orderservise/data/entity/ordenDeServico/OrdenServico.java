package br.edu.unisep.orderservise.data.entity.ordenDeServico;

import br.edu.unisep.orderservise.data.entity.carro.Carro;
import br.edu.unisep.orderservise.data.entity.cliente.Cliente;
import br.edu.unisep.orderservise.data.entity.funcionario.Funcionario;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "ordenservico")
public class OrdenServico {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "oservi_id")
    private Integer id;

    @Column(name = "decricao_os")
    private String decricao;

    @Column(name = "maodeobra_os")
    private Double maodeobra;

    @Column(name = "valor_os")
    private Double valor;

    @OneToOne
    @JoinColumn(name = "funcionario_id", referencedColumnName = "funcionario_id")
    private Funcionario funcionario;

    @OneToOne
    @JoinColumn(name = "carro_id", referencedColumnName = "carro_id")
    private Carro carro;

    //Comentado pra revisar erro

    @OneToOne
    @JoinColumn(name = "cliente_id", referencedColumnName = "cliente_id")
    private Cliente cliente;

}
