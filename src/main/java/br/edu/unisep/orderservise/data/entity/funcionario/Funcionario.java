package br.edu.unisep.orderservise.data.entity.funcionario;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "funcionarios")
public class Funcionario {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "funcionario_id")
    private Integer id;

    @Column(name = "nome_funci")
    private String nome;


}
