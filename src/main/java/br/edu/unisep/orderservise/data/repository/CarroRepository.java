package br.edu.unisep.orderservise.data.repository;

import br.edu.unisep.orderservise.data.entity.carro.Carro;
import br.edu.unisep.orderservise.data.entity.funcionario.Funcionario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CarroRepository extends JpaRepository<Carro, Integer> {

    //Optional<Carro> findByCliente(String cliente);

}
