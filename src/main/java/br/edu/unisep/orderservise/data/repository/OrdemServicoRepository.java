package br.edu.unisep.orderservise.data.repository;

import br.edu.unisep.orderservise.data.entity.ordenDeServico.OrdenServico;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrdemServicoRepository extends JpaRepository<OrdenServico, Integer> {
}
