package br.edu.unisep.orderservise.data.entity.cliente;


import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "clientes")
public class Cliente {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cliente_id")
    private Integer id;

    @Column(name = "nome_cli")
    private String nome;

    @Column(name = "fone_cli")
    private String fone;

    @Column(name = "cpf_cli")
    private String cpf;

    @Column(name = "endereco_cli")
    private String endereco;

}
