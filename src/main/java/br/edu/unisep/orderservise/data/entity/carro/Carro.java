package br.edu.unisep.orderservise.data.entity.carro;


import br.edu.unisep.orderservise.data.entity.cliente.Cliente;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "carros")
public class Carro {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "carro_id")
    private Integer id;

    @OneToOne
    @JoinColumn(name = "cliente_id", referencedColumnName = "cliente_id")
    private Cliente cliente;

    @Column(name = "modelo_car")
    private String  modelo;

    @Column(name = "placa_car")
    private String placa;


}
